function showBike(brand)
{
if (document.getElementById(brand).checked) {
  	var img = document.createElement("img");
  	img.src = './images/' + brand + '.jpeg';
  	img.className = 'brand_bike';
  	img.id = brand + "_id"
  	document.getElementById("images").append(img);
} else {
	var img = document.getElementById(brand + "_id");
	img.remove();
}
}